package com.android.task3

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(
    private val users: MutableList<User>,
    private val userListener: UserListener
) : RecyclerView.Adapter<RecyclerViewAdapter.UserViewHolder>() {

    private lateinit var name: TextView
    private lateinit var surname: TextView
    private lateinit var email: TextView
    private lateinit var image: ImageView
    private lateinit var update: ImageButton
    private lateinit var remove: ImageButton

    companion object {
        const val FIRST_NAME = "Name: "
        const val LAST_NAME = "Surname: "
        const val EMAIL = "Email: "
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.user_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount(): Int = users.size

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var userFromList: User

        @SuppressLint("SetTextI18n")
        fun bind() {
            name = itemView.findViewById(R.id.tv_name)
            surname = itemView.findViewById(R.id.tv_surname)
            email = itemView.findViewById(R.id.tv_email)
            image = itemView.findViewById(R.id.iv_image)

            update = itemView.findViewById(R.id.ib_update)
            remove = itemView.findViewById(R.id.ib_remove)

            userFromList = users[adapterPosition]

            image.setImageResource(userFromList.image ?: R.drawable.ic_face_mask)
            name.text = FIRST_NAME + userFromList.firstName
            surname.text = LAST_NAME + userFromList.lastName
            email.text = EMAIL + userFromList.email

            update.setOnClickListener {
                updateUser(adapterPosition)
            }

            remove.setOnClickListener {
                remove.isEnabled=false
                removeUser(adapterPosition)
            }

        }

        private fun updateUser(position: Int) {
            userListener.userOnUpdateListener(position)
        }

        private fun removeUser(position: Int) {
            users.removeAt(position)
            notifyItemRemoved(position)
        }
    }


}