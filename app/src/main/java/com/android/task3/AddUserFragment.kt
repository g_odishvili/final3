package com.android.task3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.task3.databinding.FragmentAddUserBinding


class AddUserFragment : Fragment() {
    private lateinit var binding: FragmentAddUserBinding;
    private lateinit var user: User
    private var parcelable: User? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentAddUserBinding.inflate(
            inflater, container, false
        )
        binding.btnSave.setOnClickListener {
            sendUpdatedUser()
        }
        init()

        return binding.root
    }


    private fun init() {
        parcelable = arguments?.getParcelable("user")
        arguments?.remove("user")
        if (parcelable != null) {
            binding.ivImage.setImageResource(parcelable!!.image ?: R.drawable.ic_android)
            binding.edFirstName.setText(parcelable!!.firstName)
            binding.edLastName.setText(parcelable!!.lastName)
            binding.edEmail.setText(parcelable!!.email)
        }

    }

    private fun setData() {
        user = User(
            parcelable?.image ?: R.drawable.ic_android,
            binding.edFirstName.text.toString().trim(),
            binding.edLastName.text.toString().trim(),
            binding.edEmail.text.toString().trim()
        )
    }

    private fun sendUpdatedUser() {
        setData()
        val bundle = bundleOf(
            "updatedUser" to user,
            "user" to parcelable,
            "usersList" to arguments?.get("usersList")
        )
        findNavController()
            .navigate(R.id.action_addUserFragment_to_userListFragment, bundle)
    }


}