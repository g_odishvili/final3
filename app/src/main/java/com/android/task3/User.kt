package com.android.task3

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var image: Int? = R.drawable.ic_face_mask,
    var firstName: String = "",
    var lastName: String = "",
    var email: String = ""
) : Parcelable
