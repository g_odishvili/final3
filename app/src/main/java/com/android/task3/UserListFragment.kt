package com.android.task3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.task3.databinding.FragmentUserListBinding

class UserListFragment : Fragment() {
    private var binding: FragmentUserListBinding? = null
    private lateinit var adapter: RecyclerViewAdapter
    private var users = mutableListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkIfUserListIsPresent()
    }

    override fun onStart() {
        super.onStart()
        checkIfUserWasAddedOrUpdated()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserListBinding.inflate(
            inflater, container, false
        )

        init()

        return binding!!.root
    }

    private fun init() {

        binding!!.btnAddUser.setOnClickListener {
            addUser()
        }

        adapter = RecyclerViewAdapter(users, object : UserListener {
            override fun userOnUpdateListener(position: Int) {
                //passing list as well for further dynamic update of list. (Could not find any other way around)
                val bundle = bundleOf("user" to users[position], "usersList" to users)

                view?.findNavController()
                    ?.navigate(R.id.action_userListFragment_to_addUserFragment, bundle)
            }

        })
        binding!!.recyclerView.layoutManager = LinearLayoutManager(binding!!.root.context)
        binding!!.recyclerView.adapter = adapter


    }

    private fun setData() {
        users.add(
            User(
                R.drawable.ic_face_mask,
                "Giorgi",
                "Odishvili",
                "odishvili.giorgi@gmail.com"
            )
        )
        users.add(
            User(
                R.drawable.ic_black_cat_face,
                "Jose ",
                "Robinson",
                "JoseHRobinson@dayrep.come"
            )
        )
        users.add(User(R.drawable.ic_animestyle, "Philipp ", "Kappel", "PhilippKappel@teleworm.us"))
        users.add(
            User(
                R.drawable.ic_annoyed_smiley_face,
                "Petra",
                "Daecher",
                "PetraDaecher@teleworm.us"
            )
        )
        users.add(
            User(
                R.drawable.ic_cute_smiley_face,
                "René",
                "Austerlitz",
                "ReneAusterlitz@teleworm.us"
            )
        )
        users.add(User(R.drawable.ic_android, "Christian", "Abt", "ChristianAbt@teleworm.us"))
        users.add(User(R.drawable.ic_ice, "Christian", "Schäfer", "ChristianSchafer@dayrep.com"))
    }

    private fun addUser() {
        view?.findNavController()?.navigate(
            R.id.action_userListFragment_to_addUserFragment,
            bundleOf("usersList" to users)
        )
    }

    //heavy but worth it
    private fun checkIfUserWasAddedOrUpdated(){
        val parcelable: User? = arguments?.getParcelable("user")
        val user: User? = arguments?.getParcelable("updatedUser")
        if (parcelable == null && user != null) {
            users.add(user)
            adapter.notifyItemInserted(users.size)
        } else if (parcelable != null && user != null) {
            val indexOf = users.indexOf(parcelable)
            users[indexOf] = user
            adapter.notifyItemChanged(indexOf)
        }
    }

    private fun checkIfUserListIsPresent() {
        val get = arguments?.get("usersList")
        if (get != null) {
            if (get is MutableList<*>)
                users = get as MutableList<User>
        } else {
            setData()
        }
    }
}